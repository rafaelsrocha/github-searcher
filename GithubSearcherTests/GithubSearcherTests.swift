//
//  GithubSearcherTests.swift
//  GithubSearcherTests
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import XCTest
@testable import GithubSearcher

class GithubSearcherTests: XCTestCase, RequestManagerDelegate {
    
    private var test: NSMutableArray?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    var expectation: XCTestExpectation?
    
    func testAPICallPerformance() {
        self.measureBlock {
            self.expectation = self.expectationWithDescription("Github API Expectation")
            
            let r = RequestManager.sharedManager()
            r.delegate = self
            r.getTopRepositories()
            
            self.waitForExpectationsWithTimeout(30, handler: { (error: NSError?) in
                XCTAssert(error == nil, "Success")
            })
        }
    }
    
    // MARK :- RequestManagerDelegate
    
    func finishedRequest(items: [AnyObject]!) {
        expectation?.fulfill()
    }
    
    func failedRequest(error: NSError!) {
        
    }
}
