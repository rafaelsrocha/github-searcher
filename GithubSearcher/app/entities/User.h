//
//  User.h
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/4/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "BaseEntity.h"

@interface User : BaseEntity
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *avatarUrl;
@end
