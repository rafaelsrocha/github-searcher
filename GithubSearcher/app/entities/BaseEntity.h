//
//  BaseEntity.h
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseEntity : NSObject
@property (strong, nonatomic) NSString *id_;
@end
