//
//  PullRequest.h
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "BaseEntity.h"
#import "User.h"

@interface PullRequest : BaseEntity
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *prDescription;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSDate *createdAt;
@property (strong, nonatomic) User *user;
@end
