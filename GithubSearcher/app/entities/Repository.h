//
//  Repository.h
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "BaseEntity.h"
#import "User.h"

@interface Repository : BaseEntity
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *repoDescription;
@property (strong, nonatomic) NSNumber *forksCount;
@property (strong, nonatomic) NSNumber *starsCount;
@property (strong, nonatomic) User *user;
@end
