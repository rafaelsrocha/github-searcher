//
//  PullRequestCell.swift
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {

    @IBOutlet var prTitleLabel: UILabel!
    @IBOutlet var prDateLabel: UILabel!
    @IBOutlet var prDescriptionLabel: UILabel!
    @IBOutlet var userPic: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userPic.layer.cornerRadius = userPic.frame.width / 2
        userPic.clipsToBounds = true
        usernameLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
