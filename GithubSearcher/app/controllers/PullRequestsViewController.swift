//
//  PullRequestsViewController.swift
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class PullRequestsViewController: UITableViewController, RequestManagerDelegate {
    private var pullRequests: NSArray?
    private let dateFormatter = NSDateFormatter()
    var repository: Repository!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = repository.name
        
        let manager = RequestManager.sharedManager()
        manager.delegate = self
        manager.getPullRequestsForRepository(repository)
        
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.show()
        }
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.registerNib(UINib(nibName: "PullRequestCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "PullRequestCell")
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pullRequests == nil || pullRequests?.count == 0 {
            let label: UILabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            label.text = "No pull requests to show. 😕"
            label.textColor = UIColor.darkGrayColor()
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.Center
            label.font = UIFont.systemFontOfSize(16.0)
            label.sizeToFit()
            tableView.backgroundView = label
            
            return 0
        }
        return pullRequests!.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PullRequestCell", forIndexPath: indexPath) as! PullRequestCell
        let pr = pullRequests![indexPath.row] as! PullRequest
        
        cell.prTitleLabel.text = pr.title
        cell.prDateLabel.text = dateFormatter.stringFromDate(pr.createdAt)
        
        if pr.prDescription != nil && pr.prDescription != "" {
            cell.prDescriptionLabel.text = pr.prDescription
            cell.prDescriptionLabel.font = UIFont.systemFontOfSize(13)
        } else {
            cell.prDescriptionLabel.text = "No description..."
            cell.prDescriptionLabel.font = UIFont.italicSystemFontOfSize(13)
        }
        
        cell.usernameLabel.text = pr.user.username
        cell.userPic.sd_setImageWithURL(NSURL(string: pr.user.avatarUrl), completed: nil)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 160
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pr = pullRequests![indexPath.row] as! PullRequest
        let url = NSURL(string: pr.url)!
        
        if UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - RequestManagerDelegate
    
    func finishedRequest(items: [AnyObject]!) {
        pullRequests = NSArray(array: items)
        tableView.reloadData()
        
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.dismiss()
        }
    }
    
    func failedRequest(error: NSError!) {
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }
    }
}
