//
//  ReposViewController.swift
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class ReposViewController: UITableViewController, RequestManagerDelegate {
    
    private var repositories: NSMutableArray?
    private var pulledToRefresh = false

    override func viewDidLoad() {
        super.viewDidLoad()

        let manager = RequestManager.sharedManager()
        manager.delegate = self
        manager.page = 1
        manager.getTopRepositories()
        
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.show()
        }
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.registerNib(UINib(nibName: "RepoCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "RepoCell")
        
        tableView.addInfiniteScrollingWithActionHandler({() -> () in
            let manager = RequestManager.sharedManager()
            manager.delegate = self
            manager.page += 1
            manager.getTopRepositories()
            
            self.pulledToRefresh = true
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        title = "GitHub JavaPop"
    }
    
    override func viewWillDisappear(animated: Bool) {
        title = ""
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if repositories == nil || repositories?.count == 0 {
            let label: UILabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            label.text = "No repositories to show. 😕"
            label.textColor = UIColor.darkGrayColor()
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.Center
            label.font = UIFont.systemFontOfSize(16.0)
            label.sizeToFit()
            tableView.backgroundView = label
            
            return 0
        }
        return repositories!.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RepoCell", forIndexPath: indexPath) as! RepoCell
        let repo = repositories![indexPath.row] as! Repository
        
        cell.nameLabel.text = repo.name
        cell.descriptionLabel.text = repo.repoDescription
        cell.branchesCountLabel.text = "\(repo.forksCount)"
        cell.starsCountLabel.text = "\(repo.starsCount)"
        
        cell.usernameLabel.text = repo.user.username
        cell.userPic.sd_setImageWithURL(NSURL(string: repo.user.avatarUrl), completed: nil)

        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let screenSize = UIScreen.mainScreen().bounds
        let repo = repositories![indexPath.row] as! Repository
        
        if repo.repoDescription != nil {
            let height = repo.repoDescription.boundingRectWithSize(CGSize(width: screenSize.size.width - 16, height: CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15)], context: nil).size.height + 100
            
            if height > 120 {
                return height
            }
        }
        
        return 120
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let repo = repositories![indexPath.row] as! Repository
        performSegueWithIdentifier("PullRequestsSegue", sender: repo)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    // MARK: - Navigation
     
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController as! PullRequestsViewController
        vc.repository = sender as! Repository
    }
    
    // MARK: - RequestManagerDelegate

    func finishedRequest(items: [AnyObject]!) {
        if pulledToRefresh {
            repositories?.addObjectsFromArray(items)
            tableView.reloadData()
            pulledToRefresh = false
            tableView.infiniteScrollingView.stopAnimating()
            return
        }
        
        repositories = NSMutableArray(array: items)
        tableView.reloadData()
        
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.dismiss()
        }
    }
    
    func failedRequest(error: NSError!) {
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }
    }
}
