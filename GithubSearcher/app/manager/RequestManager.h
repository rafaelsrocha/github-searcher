//
//  RequestManager.h
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Repository.h"
#import "PullRequest.h"
#import "User.h"

@protocol RequestManagerDelegate <NSObject>

- (void)finishedRequest:(NSArray *)items;
- (void)failedRequest:(NSError *)error;

@end

@interface RequestManager : NSObject

@property (assign, nonatomic) NSUInteger page;
@property (strong, nonatomic) id <RequestManagerDelegate> delegate;

+ (instancetype)sharedManager;
- (void)getTopRepositories;
- (void)getPullRequestsForRepository:(Repository *)repo;

@end
