//
//  RequestManager.m
//  GithubSearcher
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "RequestManager.h"

@interface RequestManager ()
@property (strong, nonatomic) RKObjectManager *objectManager;
@end

@implementation RequestManager

+ (instancetype)sharedManager {
    static RequestManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [RequestManager new];
        
        NSIndexSet *successCode = [NSIndexSet indexSetWithIndex:200];
        RKResponseDescriptor *repoDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[sharedManager repoMapping]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:@"/search/repositories"
                                                                                           keyPath:@"items"
                                                                                       statusCodes:successCode];
        
        RKResponseDescriptor *pullDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[sharedManager pullMapping]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:@"/repos/:user/:repository/pulls"
                                                                                           keyPath:nil
                                                                                       statusCodes:successCode];
        
        sharedManager.page = 1;
        sharedManager.objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"https://api.github.com"]];
        [sharedManager.objectManager addResponseDescriptor:repoDescriptor];
        [sharedManager.objectManager addResponseDescriptor:pullDescriptor];
    });
    return sharedManager;
}

- (void)getTopRepositories {
    NSString *path = [NSString stringWithFormat:@"/search/repositories?q=+language:java&sort=stars&order=desc&per_page=30&page=%lu", (unsigned long)self.page];
    
    [self.objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        [self.delegate finishedRequest:result.array];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [self.delegate failedRequest:error];
        NSLog(@"Failed with error: %@", [error localizedDescription]);
    }];
}

- (void)getPullRequestsForRepository:(Repository *)repo {
    NSString *path = [NSString stringWithFormat:@"/repos/%@/%@/pulls", repo.user.username, repo.name];
    
    [self.objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        [self.delegate finishedRequest:result.array];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [self.delegate failedRequest:error];
        NSLog(@"Failed with error: %@", [error localizedDescription]);
    }];
}

- (RKObjectMapping *)repoMapping {
    RKObjectMapping *repoMapping = [RKObjectMapping mappingForClass:Repository.class];
    [repoMapping addAttributeMappingsFromDictionary:@{
                                                      @"id" : @"id_",
                                                      @"name" : @"name",
                                                      @"description" : @"repoDescription",
                                                      @"forks_count" : @"forksCount",
                                                      @"stargazers_count" : @"starsCount"
                                                      }];
    
    [repoMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"owner" toKeyPath:@"user" withMapping:[self userMapping]]];
    return repoMapping;
}

- (RKObjectMapping *)pullMapping {
    RKObjectMapping *pullMapping = [RKObjectMapping mappingForClass:PullRequest.class];
    [pullMapping addAttributeMappingsFromDictionary:@{
                                                      @"id" : @"id_",
                                                      @"title" : @"title",
                                                      @"body" : @"prDescription",
                                                      @"created_at" : @"createdAt",
                                                      @"html_url" : @"url"
                                                      }];
    
    [pullMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"user" toKeyPath:@"user" withMapping:[self userMapping]]];
    return pullMapping;
}

- (RKObjectMapping *)userMapping {
    RKObjectMapping *userMapping = [RKObjectMapping mappingForClass:User.class];
    [userMapping addAttributeMappingsFromDictionary:@{
                                                      @"id" : @"id_",
                                                      @"login" : @"username",
                                                      @"avatar_url" : @"avatarUrl"
                                                      }];
    return userMapping;
}

@end
