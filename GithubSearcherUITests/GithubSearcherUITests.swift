//
//  GithubSearcherUITests.swift
//  GithubSearcherUITests
//
//  Created by Rafael Rocha on 4/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import XCTest

class GithubSearcherUITests: XCTestCase {
    
    private var app: XCUIApplication?
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
        app = XCUIApplication()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInfiniteScroll() {
        let table = app!.tables.elementBoundByIndex(0)
        
        sleep(3)
        
        let lastCellPage1 = table.cells.elementBoundByIndex(table.cells.count - 1)
        
        while !lastCellPage1.exists {
            table.swipeDown()
        }
        
        if lastCellPage1.exists {
            sleep(5)
            let lastCellPage2 = table.cells.elementBoundByIndex(table.cells.count - 1)
            XCTAssertNotEqual(lastCellPage1, lastCellPage2, "Last cell of page 1 and last cell of page 2 shouldn't be equal")
        } else {
            XCTFail("Last Cell of Page 1 doesn't exist")
        }
    }
    
    func testCellTap() {
        let table = app!.tables.elementBoundByIndex(0)
        
        sleep(3)
        table.cells.elementBoundByIndex(0).tap()
        XCTAssert(app!.navigationBars["elasticsearch"].exists)
    }
}
